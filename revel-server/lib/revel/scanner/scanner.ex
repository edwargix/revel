defmodule Revel.Scanner do
  require Logger
  use Task
  use Rustler, otp_app: :revel, crate: :revel_scanner

  def run(database, parent) do
    scan_path = Revel.Config.get_val("Paths", "music_path")
    :ok = update_database(database, scan_path, 4)

    send(parent, :scan_complete)
  end

  def create_database(_db_path, _scan_path, _num_threads), do: :erlang.nif_error(:nif_not_loaded)
  
  def update_database(_db_path, _scan_path, _num_threads), do: :erlang.nif_error(:nif_not_loaded)
  
end
