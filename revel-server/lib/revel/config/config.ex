defmodule Revel.Config do
  require Logger
  use GenServer

  @moduledoc """
  GenServer module to handle config file access
  """

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, [], name: :config)
  end

  def get_val(section, key) do
    GenServer.call(:config, {:get_val, {section, key}})
  end

  def handle_call({:get_val, {section, key}}, _from, config) do
    val = config[section][key]
    {:reply, val, config}
  end

  defp create_default_config(config_file) do
    #default_config = """
    #  [Paths]
    #  # Paths ##########################################################
    #  #
    #  # This directory is where revel will index and serve music from.
    #  #
    #  music_path = /path/to/music
    #  #
    #  # This directory sets the location where revel will temporaily
    #  # store converted files
    #  # If unset, will default to $XDG_CACHE_HOME
    #  tmp_dir =

    #  [Network]
    #  # Network ########################################################
    #  #
    #  listen_ip = 127.0.0.1
    #  listen_port = 32320
    #  # This is the web path that the revel server will be accessed at.
    #  # If your server is available at http://example.com/revel, path
    #  # should be set to /revel
    #  path = /
    #"""

    default_config = Application.fetch_env!(:revel, :default_config)

    File.write(config_file, default_config)
  end

  defp load_config_file(config_dir) do
    config_file = Path.join([config_dir, "config.ini"])

    case File.stat(config_file) do
      {:ok, _stat} ->
        Logger.info("Loading configuration from " <> config_file)
        {:ok, config} = ConfigParser.parse_file(config_file)
        Logger.info("Config file " <> config_file <> " loaded sucessfully")
        config

      {:error, :enoent} ->
        case File.stat(config_dir) do
          {:error, :enoent} ->
            Logger.info("Creating configuration folder at " <> config_dir)
            File.mkdir_p(config_dir)

          {:error, error} ->
            raise error

          {:ok, _stat} ->
            nil
        end

        Logger.info("Creating default config file at " <> config_file)
        create_default_config(config_file)
        Logger.info("Loading configuration from " <> config_file)
        {:ok, config} = ConfigParser.parse_file(config_file)
        Logger.info("Config file " <> config_file <> " loaded sucessfully")
        config
    end
  end

  def init(_constant) do
    xdg_config = System.get_env("XDG_CONFIG_HOME")
    home = System.get_env("HOME")

    cond do
      xdg_config != nil ->
        Logger.debug("Locating config file using $XDG_CONFIG_HOME (" <> xdg_config <> ")")
        config_dir = Path.join([xdg_config, "revel"])
        config = load_config_file(config_dir)
        {:ok, config}

      home != nil ->
        Logger.debug("Locating config file using $HOME (" <> home <> ")")
        config_dir = Path.join([home, ".config", "revel"])
        config = load_config_file(config_dir)
        {:ok, config}
    end
  end
end
