Revel: A sonic Server Written In Elixir
========================================
DISCLAIMER: This software is a WIP, don't use it in production please
---------------------------------------------------------------------
revel-indexer
-------------
revel-indexer is licenced under the GPLv3.

revel-indexer is written in C++. A C++ compiler (gcc here) is required to
compile revel-indexer. Other required dependencies are ``SQLite`` and
``TagLib``.

To compile the indexer component of revel:

.. code:: bash

    gcc indexer.cpp -Wall -std=c++11 -ltag -lstdc++ -lsqlite3 -g -o revel_indexer

To scan a directory:

.. code:: bash

    ./revel_indexer scan database.db /mnt/music

The database should be moved to the `revel-server` directory after generation.

revel-server
------------
revel-server is licenced under the AGPLv3.

revel-server is written in Elixir. Elixir must be installed to run it in its
current form. Get Elixir from wherever packages are sold near you (typically
your GNU/Linux distribution's package manager).

To run the server component of revel do the following:

.. code:: bash

    cd revel-server
    mix deps.get
    mix compile && mix run --no-halt

On first run, revel-server will create a configuration file in
``$XDG_CONFIG_HOME/revel`` (``$HOME/.config/revel`` if ``$XDG_CONFIG_HOME`` is
not defined). Listen ip and port can be configured in this file (default
values are ``listen_ip = 127.0.0.1`` and ``listen_port = 32320``).

To run the server component and open `iex` for debugging run:

.. code:: bash
    
    mix compile && iex -S mix

revel-config
------------
revel-config is licenced under the GPLv3

revel-config is written in Python 3. Python 3 must be installed to run it in
its current from.

revel-config serves as a simple command line configuration manager for revel.
Basic features include pinging the server to check if it is accesible and
creating a new user account.
